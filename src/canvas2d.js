const animate = canvas => {
  // let count = 30;

  const times = [];
  const deltas = [];
  let lastTime = 0;

  const onFrame = time => {
    const seconds = (time / 1000) * Math.PI / 5;

    const gl = canvas.getContext('webgl');
    const context2d = canvas.getContext('2d');

    window.gl = gl;

    if(gl) {
      window.gl = gl;
      gl.clearColor(1, 1 / 3, 0, 1);
      gl.clear(gl.COLOR_BUFFER_BIT);
    }

    if(context2d) {
      window.context2d = context2d;
      const { width, height } = canvas;
      context2d.clearRect(0, 0, width, height);
      context2d.fillStyle = '#88000088';
      context2d.fillRect(Math.cos(seconds) * ((width / 2) - 20) + (width / 2) - 5, 10, 10, 40);

      times.push(time);
      deltas.push(time - lastTime);
      lastTime = time;
    }

    // if(count--)
      window.requestAnimationFrame(onFrame);
  };

  window.requestAnimationFrame(onFrame);
};
