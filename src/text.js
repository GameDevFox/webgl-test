export const buildTextCanvas = (text, {
  size = 10, margin = 0, border,
  font = 'sans-serif', color, background,
} = {}) => {
  const canvas = document.createElement('canvas');
  const context2d = canvas.getContext('2d');

  context2d.font = `${size}px ${font}`;
  const { width } = context2d.measureText(text);

  const arr128 = [];
  for(let i = 0; i < 128; i++)
    arr128.push(i);
  const allChars = Buffer.from(arr128).toString('utf8');
  const sizes = [];
  allChars.split('').forEach(char => {
    const size = context2d.measureText(char).width;
    sizes.push(size);
  });
  console.log('S', sizes);
  const first = sizes[0];
  const result = sizes.filter(size => size !== first);
  console.log('R', result);

  canvas.width = width + margin;
  canvas.height = size + margin;

  context2d.font = `${size}px ${font}`;
  context2d.textAlign = 'center';
  context2d.textBaseline = 'middle';

  if(background) {
    const oldStyle = context2d.fillStyle;
    context2d.fillStyle = background;
    context2d.fillRect(0.5, 0.5, canvas.width - 1, canvas.height - 1);
    context2d.fillStyle = oldStyle;
  }

  context2d.fillStyle = color;
  context2d.fillText(text, canvas.width / 2, canvas.height / 2);

  if(border) {
    context2d.strokeStyle = border;
    context2d.strokeRect(0.5, 0.5, canvas.width - 1, canvas.height - 1);
  }

  return canvas;
};
