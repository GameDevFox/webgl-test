import React from 'react';
import { render } from 'react-dom';
import { createGlobalStyle } from 'styled-components';

import App from './app';

const BodyStyle = createGlobalStyle`
  body { background-color: lightyellow; }

  #my-canvas {
    display: block;
    position: absolute;
    top: 0px;
    left: 0px;
  }
`;

const app = (
  <>
    <BodyStyle/>
    <App/>
  </>
);

// Render App
render(app, document.getElementById('root'));
