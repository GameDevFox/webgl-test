export const on = (...args) => {
  if(args.length === 2)
    args = [window, ...args];

  const [target, eventName, fn] = args;

  target.addEventListener(eventName, fn);
};
