/* eslint-disable */
import React, { useEffect, useRef, useDebugValue } from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import styled, { createGlobalStyle } from 'styled-components';

import './webgl';

import store from './store';

const Style = styled.span`
  margin: 14px;
  display: inline-block;

  .canvas-box {
    canvas {
      border: 1px solid blue;
    }

    position: absolute;
    top: 5px;
    left: 5px;
  }
`;

const App = () => {
  const canvasRef = useRef(null);

  return (
    <Provider store={store}>
      <h1>Hello World Again</h1>
      <ul>
        <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</li>
        <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</li>
        <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</li>
        <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</li>
      </ul>
      {/*
      <Style>
        <h1>Hello World</h1>
        <h1></h1>
        <h1>Hello World</h1>
        <h1>Na na na na na na na na na, na na na na na</h1>
        <h1>
          .multiplyVectors ( a : Vector3, b : Vector3 ) : this
          Sets this vector equal to a * b, component-wise.
        </h1>
        <div className="canvas-box">
          <canvas ref={canvasRef} width={width} height={height}/>
          <div><button onClick={() => animate(canvasRef.current)}>Draw</button></div>
        </div>
      </Style>
      */}
    </Provider>
  );
};

export default hot(App);
