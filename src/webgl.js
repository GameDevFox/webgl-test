/* eslint-disable */
import {
  BoxGeometry, Mesh, MeshNormalMaterial, PerspectiveCamera, Scene, WebGLRenderer,
  Quaternion, Vector3, Object3D, Euler, CanvasTexture, MeshBasicMaterial,
  PlaneGeometry,
} from 'three';
import { WEBVR } from 'three/examples/jsm/vr/WebVR';
import 'three/examples/js/vr/HelioWebXRPolyfill.js';

import { buildTextCanvas } from './text';
import { on } from './utils';

const otherMsg = 'May Zion Prosper';
const textCanvas = buildTextCanvas(otherMsg, {
  color: 'white', /*background: 'black',*/
  font: 'monospace', margin: 4, size: 50,
});
const textTex = new CanvasTexture(textCanvas);
// document.body.appendChild(textCanvas);

const textureMat = new MeshBasicMaterial({ map: textTex });
textureMat.transparent = true;

const launchApp = canvas => {
  const keys = {};
  on('keydown', e => (keys[e.keyCode] = true));
  on('keyup', e => (keys[e.keyCode] = false));
  const isKeyDown = keyCode => keys[keyCode] === true;

  const scene = new Scene();

  const rig = new Object3D();
  rig.position.z = 1;
  scene.add(rig);

  const aspectRatio = window.innerWidth / window.innerHeight;
  const camera = new PerspectiveCamera(70, aspectRatio, 0.01, 10);
  window.camera = camera;
  // const camera = new PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 10);
  rig.add(camera);

  const geometry = new BoxGeometry(0.2, 0.2, 0.2);
  const material = new MeshNormalMaterial();
  // material.transparent = true;
  // material.opacity = 0.5;

  // const mesh = new Mesh(geometry, material);
  const mesh = new Mesh(geometry, material);
  scene.add(mesh);


  const consoleGeom = new PlaneGeometry(textCanvas.width / 100, textCanvas.height / 100);
  window.consoleGeom = consoleGeom;
  const consoleMaterial = textureMat;
  const consoleMesh = new Mesh(consoleGeom, consoleMaterial);
  // consoleMesh.position.y = 0.5;
  consoleMesh.position.z = -0.5;
  scene.add(consoleMesh);

  const renderer = new WebGLRenderer({ canvas, alpha: true, antialias: true });
  // const renderer = new WebGLRenderer({ antialias: true  });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.vr.enabled = true;

  const smallGeometry = new BoxGeometry(0.1, 0.1, 0.1);

  const leftController = renderer.vr.getController(0);
  const leftMesh = new Mesh(smallGeometry, material);
  leftController.add(leftMesh);
  rig.add(leftController);

  const rightController = renderer.vr.getController(1);
  const rightMesh = new Mesh(smallGeometry, material);
  rightController.add(rightMesh);
  rig.add(rightController);

  window.renderer = renderer;

  // document.body.appendChild(renderer.domElement);
  document.body.appendChild(WEBVR.createButton(renderer));

  let pointerLock = false;
  document.addEventListener('pointerlockchange', e => {
    pointerLock = !(e.currentTarget.pointerLockElement === null);
  });

  renderer.setAnimationLoop(time => {
    // mesh.position.y = 1.6;

    // mesh.position.z += 0.001;

    // mesh.rotation.x += 0.01;
    // mesh.rotation.y += 0.01;
    // mesh.rotation.z += 0.01;

    const [UP, DOWN, LEFT, RIGHT] = [87, 83, 65, 68];
    // if(pointerLock) {
      const worldDir = new Vector3();
      camera.getWorldDirection(worldDir);
      const sideWorldDir = worldDir.clone().applyEuler(new Euler(0, Math.PI / 2, 0));
      // console.log('SWD', sideWorldDir);

      const rightDir = new Vector3(1, 0, 0);
      rightDir.applyQuaternion(rig.quaternion);
      // const rightQuat = new Quaternion();
      // rightQuat.setFromEuler(new Euler(0, Math.PI / 2, 0));

      const distance = 0.05;

      const gamepads = navigator.getGamepads();
      const [leftGamepad, rightGamepad] = gamepads;
      if(leftGamepad) {
        // const [axisX, axisY, viewX, viewY] = leftGamepad.axes;
        const [axisX, axisY] = leftGamepad.axes;
        const [viewX, viewY] = rightGamepad.axes;
        const [
          leftStick, leftTrigger, leftGrip, leftBottomX, leftTopY,
        ] = leftGamepad.buttons;
        const [
          rightStick, rightTrigger, rightGrip, rightBottomA, rightTopB,
        ] = rightGamepad.buttons;

        mesh.position.x = 0;
        mesh.position.y = 0;
        mesh.position.z = 0;

        const thisButton = rightBottomA;
        if(thisButton.pressed) {
          mesh.position.y = 0.50;
        } else if(thisButton.touched) {
          mesh.position.y = 0.25;
        }

        // const target = [leftGamepad.pose];
        // const myCount = target.length;
        // const index = (Math.floor(time / 500) % (myCount + 1));
        // mesh.position.x = index * 0.2;
        // if(target[index] === undefined)
        //   mesh.position.y -= 0.25;

        if(Math.abs(axisX) > 0.1) {
          // console.log('Axis X:', axisX);
          mesh.rotation.y += (0.1 * axisX);
        }

        if(Math.abs(axisY) > 0.1) {
          // console.log('Axis Y:', axisY);
          mesh.rotation.x += (0.1 * axisY);
        }

        // if(Math.abs(viewX) > 0.1) {
          mesh.position.x += viewX;
          // rig.rotateOnAxis(new Vector3(0, 1, 0), -viewX / 50);
        // }

        // if(Math.abs(viewY) > 0.1) {
          mesh.position.z += viewY;
          // rig.rotateOnAxis(new Vector3(1, 0, 0), -viewY / 50);
        // }
      }

      if(isKeyDown(UP))
        rig.position.addScaledVector(worldDir, distance);

      if(isKeyDown(DOWN))
        rig.position.addScaledVector(worldDir, -distance);

      if(isKeyDown(LEFT))
        rig.position.addScaledVector(rightDir, -distance);

      if(isKeyDown(RIGHT))
        rig.position.addScaledVector(rightDir, distance);
    // }

    renderer.render(scene, camera);
  });

  on('resize', () => {
    camera.aspect = (window.innerWidth / window.innerHeight);
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  });

  on('mousemove', e => {
    if(!pointerLock)
      return;

    rig.rotateOnAxis(new Vector3(0, 1, 0), -(e.movementX / 1000));
    rig.rotateOnAxis(new Vector3(1, 0, 0), -(e.movementY / 1000));

    // console.log(camera.position, rig.position);
    // console.log(camera.getWorldPosition(new Vector3()), rig.getWorldPosition(new Vector3()));
  });

  on(renderer.domElement, 'click', e => {
    if(pointerLock)
      return;

    e.currentTarget.requestPointerLock();
  });

  // const animateGL = () => {
  //   requestAnimationFrame(animateGL);
  //
  //   mesh.rotation.x += 0.01;
  //   mesh.rotation.y += 0.05;
  //
  //   renderer.render(scene, camera);
  //
  //   window.info = renderer.info;
  // };
  //
  // animateGL();
};

const canvas = document.querySelector('#my-canvas');
launchApp(canvas);
